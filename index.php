<!DOCTYPE html>
<html>
<head>
  <title>Wordpress Blog</title>
  <link href="<?php echo get_bloginfo('template_directory'); ?>/blog.css" rel="stylesheet" type="text/css">
</head>
<body>

<header>
</header>
 
<nav>
    <ul>
      <li>Aktuelles</li>
      <li>Aktuelleres</li>
      <li>Das neuste</li>
      <li>Archiv</li>
      <li>Politische Arbeit</li>
      <li>Kontakt</li>
      <li>Impressum</li>
    </ul>
</nav>
 
<main>
<h1>Blog-Einträge</h1>:
  <article>
    <h2>Blog-Eintrag 2</h2>
    <span>31.02.2019</span>
    Hallo, ich wollte hier etwas schreiben, habe aber eigentlich keine Ahnung, was. Sorry, dass ich so wenig poste...
    <a href="mehrlesen.php">Mehr lesen</a>
  </article>
  <article>
    <h2>Blog-Eintrag 1</h2>
    <span>01.01.1970</span>
    Moin Moin, ich habe jetzt einen Blog! Hier kommen jeden Tag neue Einträge und auch mal längere seiten zu sehr wichtigen Themen
    <a href="mehrlesen.php">Mehr lesen</a>
  </article>
</main>

<footer>
Proudly created with <a href="https://www.phase5.info/">Phase 5 '99</a> <a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-html401" alt="Valid HTML 4.01 Strict" height="31" width="88"></a>
</footer>

</body>
</html>
