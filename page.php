<!DOCTYPE html>
<html>
<head>
<title>Worpress Blog</title>
</head>
<body>

<header>
<h1>Wordpress 🅱️lag</h1>
</header>

<nav>
  <ul>
    <li>Aktuelles</li>
    <li>Aktuelleres</li>
    <li>Das neuste</li>
    <li>Archiv</li>
    <li>Politische Arbeit</li>
    <li>Kontakt</li>
    <li>Impressum</li>
  </ul>
</nav>
 
<main>
<h1>Einseitiger Artikel</h1>
  <article>
    <h2>Warum ich nicht mehr schreibe</h2>
    <p>Moin Loide, ihr wisst ja, normalerweise lasse ich es mit dem Schreiben nicht so schleifen (höhö) aber dieses Mal muss ich leider einfach einen Blindtext nehmen. 
    </p>
    <p>
    Zum Nikolaus ein kleines Bonbon: Jeder Webworker braucht Blindtexte, um ein Print- oder Weblayout schnell mit Text zu füllen. Blindtexte, die ab und zu tatsächlich gelesen werden. Warum Kunden mit dem alten »Lorem Ipsum« langweilen, wenn wir den Platz auch nutzen könnten, um für Webstandards zu werben, fragte sich Nicolai Schwarz. Und so bieten wir euch heute einen alternativen Blindtext für Standard-Fans.

Lorem Ipsum

Überall dieselbe alte Leier. Das Layout ist fertig, der Text lässt auf sich warten. Damit das Layout nun nicht nackt im Raume steht und sich klein und leer vorkommt, springe ich ein: der Blindtext. Genau zu diesem Zwecke erschaffen, immer im Schatten meines großen Bruders »Lorem Ipsum«, freue ich mich jedes Mal, wenn Sie ein paar Zeilen lesen. Denn esse est percipi – Sein ist wahrgenommen werden.
Und weil Sie nun schon die Güte haben, mich ein paar weitere Sätze lang zu begleiten, möchte ich diese Gelegenheit nutzen, Ihnen nicht nur als Lückenfüller zu dienen, sondern auf etwas hinzuweisen, das es ebenso verdient wahrgenommen zu werden: Webstandards nämlich.
</p>
  </article>
</main>

<footer>
Proudly created with <a href="https://www.phase5.info/">Phase 5 '99</a> <a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-html401" alt="Valid HTML 4.01 Strict" height="31" width="88"></a>
</footer>

</body>
</html>
